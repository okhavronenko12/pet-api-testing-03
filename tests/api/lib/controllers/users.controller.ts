import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class UsersController {
    async getAllUsers() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`api/Users`).send();
        return response;
    }

    async getUserById(id) {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`api/Users/${id}`).send();
        return response;
    }

    async getCurrentUser(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async updateUser(userData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("PUT")
            .url(`api/Users`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async delete(id: string, token: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("DELETE")
            .url(`api/Users/${id}`)
            .bearerToken(token)
            .send();
        return response;
    }
}
