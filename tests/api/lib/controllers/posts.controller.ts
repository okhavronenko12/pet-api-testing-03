import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`api/Posts`).send();
        return response;
    }

    async create(
        post: {
            authorId: number;
            body: string;
        },
        token: string
    ) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(post)
            .bearerToken(token)
            .send();
        return response;
    }

    async setLike(postId: number, userId: number, token: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId: postId,
                isLike: true,
                userId: userId,
            })
            .bearerToken(token)
            .send();
        return response;
    }
}
