import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class AuthController {
    async login(emailValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email: emailValue,
                password: passwordValue,
            })
            .send();
        return response;
    }

    async register(data: { email: string; password: string; userName: string }) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body(data)
            .send();
        return response;
    }
}
