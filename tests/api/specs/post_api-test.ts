import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";

const auth = new AuthController();
const posts = new PostsController();

const user = {
    userName: "Olga",
    email: "olgakha1v@gmail.com",
    password: "11223344",
};

const postData = {
    previewImage: "previewImage-string",
    body: "body-string",
};

let userRes = {};
let postRes = {};

let token = "";

describe("Posts usage", () => {
    before(`Login and get the token`, async () => {
        let response = await auth.login(user.email, user.password);

        token = response.body.token.accessToken.token;
        userRes = response.body.user;
    });

    it(`Create post`, async () => {
        const response = await posts.create({ authorId: userRes["id"], ...postData }, token);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);

        postRes = response.body;
    });

    it(`Get all posts`, async () => {
        const response = await posts.getAllPosts();
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.body, `Response should be array`).to.be.an("array");
    });

    it(`Set post like`, async () => {
        const response = await posts.setLike(postRes["id"], user["id"], token);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });
});
