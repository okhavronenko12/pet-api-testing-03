import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const users = new UsersController();
const auth = new AuthController();

const user = {
    userName: "Olga",
    email: "olgakha1v@gmail.com",
    password: "11223344",
};

let userRes = {};

let token = "";

describe("Auth usage", () => {
    it(`Check register`, async () => {
        const response = await auth.register(user);
        expect(response.statusCode, `Status Code should be 201`).to.be.equal(201);
        expect(response.body.token, `Response should be contains token`).not.to.be.empty;
    });

    it(`Get all users`, async () => {
        const response = await users.getAllUsers();
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.body, `Response should be array`).to.be.an("array");
    });

    it(`Check login`, async () => {
        const response = await auth.login(user.email, user.password);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.body.token, `Response should be contains token`).not.to.be.empty;
        token = response.body.token.accessToken.token;
        userRes = response.body.user;
    });

    it(`Get current user`, async () => {
        const response = await users.getCurrentUser(token);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.body, `Response should be object`).to.be.an("object");
    });

    it(`Update current user`, async () => {
        const response = await users.updateUser({ id: userRes["id"], userName: "OLGA" }, token);
        expect(response.statusCode, `Status Code should be 204`).to.be.equal(204);
    });

    it(`Get current user with new data`, async () => {
        const response = await users.getCurrentUser(token);
        console.log(response)
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.body.userName, `Response should be contains userName as OLGA`).to.be.equal("OLGA");
    });

    it(`Get user by id`, async () => {
        const response = await users.getUserById(userRes["id"]);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.body.userName, `Response should be contains userName as OLGA`).to.be.equal("OLGA");
    });

    it(`Delete user by id`, async () => {
        const response = await users.delete(userRes["id"], token);
        expect(response.statusCode, `Status Code should be 204`).to.be.equal(204);
    });
});
